import {Component} from "@angular/core";
import {FibonacciService} from "./fibonacci.service";

@Component({
  selector: "fibonacci",
  template: `
    <div>
      Argument: <input type="number" min="0" [(ngModel)]="argument"/>
      <button type="button" (click)="compute()">Compute</button><br/>
      <span>Fibonacci({{argument}}) = {{result | number:'1.0-0'}}</span>
    </div>
  `,
  styles: []
})
export class FibonacciComponent {
  argument: number = 10;
  result: number   = 0;
  constructor(private svc: FibonacciService) {}
  compute(): void {
    this.result = this.svc.compute(this.argument);
  }
}

