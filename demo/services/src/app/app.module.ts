import {BrowserModule} from "@angular/platform-browser";
import {InjectionToken, NgModule} from "@angular/core";

import {AppComponent} from "./app.component";
import {FibonacciComponent} from "./fibonacci.component";
import {FibonacciService, IterativeFibonacci, RecursiveFibonacci} from "./fibonacci.service";
import {FormsModule} from "@angular/forms";
import {UsingValuesComponent} from "./using-values.component";
import {AWS, BACKEND_URI} from "./app.consts";


@NgModule({
  declarations: [AppComponent, FibonacciComponent, UsingValuesComponent],
  imports: [BrowserModule, FormsModule],
  // providers: [FibonacciService],
  providers: [
    {provide: FibonacciService, useClass: IterativeFibonacci},
    {provide: BACKEND_URI, useValue: "https://api.ribomation.se/v2"},
    {provide: AWS, useValue: Object.freeze({
        accessKey:'0x12121212',
        secrectKey: '0x424242424242'
      })},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
