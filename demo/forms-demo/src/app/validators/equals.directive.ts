import {Directive, Input} from '@angular/core';
import {
  FormGroup, NG_VALIDATORS,
  ValidationErrors, Validator
}                         from '@angular/forms';
import {equals}           from './user-validators';

@Directive({
  selector:  '[equals]',
  providers: [
    {provide: NG_VALIDATORS, useExisting: EqualsDirective, multi: true}
  ]
})
export class EqualsDirective implements Validator {
  @Input('equals') fieldNames: string[] = [];

  validate(form: FormGroup): ValidationErrors | null {
    return equals(this.fieldNames[0], this.fieldNames[1])(form);
  }
}



