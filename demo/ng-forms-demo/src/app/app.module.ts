import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentOrientedFormComponent } from './component-oriented-form/component-oriented-form.component';
import { TemplateOrientedFormComponent } from './template-oriented-form/template-oriented-form.component';

@NgModule({
  declarations: [
    AppComponent,
    ComponentOrientedFormComponent,
    TemplateOrientedFormComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
