import {Component} from "@angular/core";
import {Router} from "@angular/router";
import {AuthService, User} from "./auth.service";
import {Observable} from "rxjs/Observable";

@Component({
  selector: "app-root",
  template: `
    <h1>{{title}}</h1>
    
    <div *ngIf="loggedin; else mustLogin">
      User {{username}} logged in.
      <button type="button" (click)="logout()">LOGOUT</button>
    </div>
    <ng-template #mustLogin>
      Please <a routerLink="/login">LOGIN</a>
    </ng-template>
    
    <div>
      <!--<h2>Navigation via links</h2>-->
      <a routerLink="/first" routerLinkActive="markIt">[First]</a>
      <a routerLink="/second" routerLinkActive="markIt">[Second]</a>
      <a routerLink="/third" routerLinkActive="markIt">[Third]</a>
      <a [routerLink]="['/detail', 'FooBar']" routerLinkActive="markIt">[Detail]</a>
      <a routerLink="/fake" routerLinkActive="markIt">[Data]</a>
      <a routerLink="/products" routerLinkActive="markIt">[Products]</a>
      <a routerLink="/admin" routerLinkActive="markIt">[ADMIN]</a>
    </div>
    <!--<div>
      <h2>Navigation via router</h2>
      <button type="button" (click)="goto('first')">Goto First</button>
      <button type="button" (click)="goto('second')">Goto Second</button>
      <button type="button" (click)="goto('third')">Goto Third</button>
    </div>-->
    <div id="content">
      <router-outlet></router-outlet>
    </div>
  `,
  styles: [`
    #content {
      border: solid 2px gray;
      margin-top: 1rem;
    }

    .markIt {
      background-color: orange;
    }
  `]
})
export class AppComponent {
  title = "Simple Routing Demo App";
  loggedin: boolean = false;
  username: string;

  constructor(private router: Router, private auth: AuthService) {
    auth.authFeed().subscribe(u => {
      console.log("[app]", "auth", u);
      if (u.authenticated) {
        this.loggedin = true;
        this.username = u.username;
      } else {
        this.loggedin = false;
        this.username = null;
      }
    });
  }

  logout(): void {
    this.auth.logout();
    this.goto("/login");
  }

  goto(page: string): void {
    this.router.navigate([`/${page}`]);
  }
}
