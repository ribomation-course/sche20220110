import {Component} from '@angular/core';
import {Person} from "./person.domain";

const PERSONS: Person[] = [
    {
        "first_name": "Ellary",
        "last_name": "D'Onisi",
        "email": "edonisi0@networkadvertising.org"
    },
    {
        "first_name": "Marybelle",
        "last_name": "Sharpous",
        "email": "msharpous1@parallels.com"
    },
    {
        "first_name": "Carol",
        "last_name": "Rackley",
        "email": "crackley2@indiegogo.com"
    },
    {
        "first_name": "Gusti",
        "last_name": "Spat",
        "email": "gspat3@unicef.org"
    },
    {
        "first_name": "Thoma",
        "last_name": "Ensten",
        "email": "tensten4@wix.com"
    },
    {
        "first_name": "Cristie",
        "last_name": "Wedlake",
        "email": "cwedlake5@comcast.net"
    },
    {
        "first_name": "Joy",
        "last_name": "Chaudron",
        "email": "jchaudron6@arstechnica.com"
    },
    {
        "first_name": "Rani",
        "last_name": "Masser",
        "email": "rmasser7@hc360.com"
    },
    {
        "first_name": "Cybil",
        "last_name": "Hambrick",
        "email": "chambrick8@hubpages.com"
    },
    {
        "first_name": "Cacilia",
        "last_name": "Grigg",
        "email": "cgrigg9@goo.ne.jp"
    }
];

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    persons: Person[] | undefined = PERSONS;

    toggle() {
        if (this.persons) {
            this.persons = undefined;
        } else {
            this.persons = PERSONS;
        }
    }
}
