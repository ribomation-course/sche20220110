export class Product {
    constructor(
        public id: number = -1,
        public maker: string = '',
        public model: string = '',
        public year: number = 0,
        public price: number = 0,
    ) {
    }

    static fromDB(data: any): Product {
        return Object.assign(new Product(), data);
    }

    toDB(): any {
        return Object.assign({}, this);
    }

}
