import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {lastValueFrom, map, Observable, take} from "rxjs";
import {Product} from "../domain/product.domain";

@Injectable({
    providedIn: 'root'
})
export class ProductDaoService {
    readonly baseUrl = 'http://localhost:3000/products';

    constructor(
        private http: HttpClient
    ) {
    }

    all(): Promise<Product[] | undefined> {
        return this.http
            .get<Product[]>(this.baseUrl)
            .pipe(
                map(lst => lst.map(p => Product.fromDB(p))),
                take(1)
            )
            .toPromise(); //deprecated from RxJS v7
    }

    one(id: number): Promise<Product> {
        const result$ = this.http
            .get<Product>(`${this.baseUrl}/${id}`)
            .pipe(map(p => Product.fromDB(p)))
        return lastValueFrom(result$); //recommended as replacement for .toPromise()
    }

    update(id: number, propertyName: string, propertyValue: any): Promise<Product> {
        const payload: any = {};
        payload[propertyName] = propertyValue;
        const result$= this.http
            .patch<Product>(`${this.baseUrl}/${id}`, payload)
            .pipe(map(p => Product.fromDB(p)))
        return lastValueFrom(result$);
    }

    remove(id: number): Promise<void> {
        return lastValueFrom(this.http
            .delete<void>(`${this.baseUrl}/${id}`));
    }

    create(data: Product): Promise<Product> {
        return lastValueFrom(this.http
            .post(this.baseUrl, data.toDB())
            .pipe(map(p => Product.fromDB(p))));
    }

}
