import {Component} from '@angular/core';
import {ProductDaoService} from "./services/product-dao.service";
import {Product} from "./domain/product.domain";
import {Property} from "./widgets/editable/property";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    products: Product[] | undefined = [];
    product: Product | undefined = undefined;

    constructor(
        public productsDAO: ProductDaoService
    ) {
        this.load().then(_ => true);
    }

    async load() {
        this.products = await this.productsDAO.all();
    }

    async show(id: number) {
        this.product = await this.productsDAO.one(id);
    }

    async save(id: number, property: Property) {
        console.log(property);
        this.product = await this.productsDAO
            .update(id, property.name, property.value);
        await this.load();
    }

    async remove(id: number) {
        await this.productsDAO.remove(id)
        this.clear();
        await this.load();
    }

    clear() {
        this.product = undefined;
    }

}
