import {Component} from '@angular/core';
import {AbstractEditable} from "../abstract-editable";

@Component({
    selector: 'editable-text',
    templateUrl: '../abstract-editable.html',
    styleUrls: ['../abstract-editable.scss']
})
export class EditableTextWidget extends AbstractEditable<string>  {
}
