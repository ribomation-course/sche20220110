import {Component} from '@angular/core';
import {AbstractEditable} from "../abstract-editable";

@Component({
    selector: 'editable-number',
    templateUrl: '../abstract-editable.html',
    styleUrls: ['../abstract-editable.scss']
})
export class EditableNumberWidget extends AbstractEditable<number> {
}
