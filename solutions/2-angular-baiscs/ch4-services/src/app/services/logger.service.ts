import {Injectable} from '@angular/core';
import {formatDate} from '@angular/common'

const ALL = 0;
const DEBUG = 1;
const WARN = 5;
const ERROR = 10;
const NONE = 100;

@Injectable({
    providedIn: 'root'
})
export class LoggerService {
    name: string = 'logger';
    level: number = DEBUG;

    constructor() {
    }

    debug(msg: string) {
        if (this.level > DEBUG) return;
        this.log(msg);
    }

    warning(msg: string) {
        if (this.level > WARN) return;
        this.log(msg);
    }

    error(msg: string) {
        if (this.level > ERROR) return;
        this.log(msg);
    }

    private log(msg: string) {
        console.log('%s [%s] %s - %s',
            formatDate(new Date(), 'HH:mm:ss', 'en'),
            this.name,
            this.toString(this.level),
            msg
        );
    }

    private toString(level: number): string {
        switch (level) {
            case DEBUG:
                return 'DEBUG';
            case WARN:
                return 'WARN';
            case ERROR:
                return 'ERROR';
        }
        return '*';
    }

}
