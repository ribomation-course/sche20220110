import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {LoggerService} from "./services/logger.service";

const loggerFactory = () => {
    let service = new LoggerService();
    service.level = 10;
    service.name = 'tjollahopp'
    return service;
}

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule
    ],
    providers: [
        {
            provide: LoggerService, useFactory: loggerFactory
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
