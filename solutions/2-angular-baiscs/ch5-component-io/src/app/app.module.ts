import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {NumberStepperWidget} from './widgets/number-stepper/number-stepper.widget';
import {FormsModule} from "@angular/forms";

@NgModule({
    declarations: [
        AppComponent,
        NumberStepperWidget
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
