import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NumberStepperWidget } from './number-stepper.widget';

describe('NumberStepperWidget', () => {
  let component: NumberStepperWidget;
  let fixture: ComponentFixture<NumberStepperWidget>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NumberStepperWidget ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NumberStepperWidget);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
