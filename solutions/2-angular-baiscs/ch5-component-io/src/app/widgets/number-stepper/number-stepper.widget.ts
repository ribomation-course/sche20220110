import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'number-stepper',
    templateUrl: './number-stepper.widget.html',
    styleUrls: ['./number-stepper.widget.css']
})
export class NumberStepperWidget {
    @Output('valueChange') out = new EventEmitter<number>();
    @Input('value') currentValue: number = 1;
    @Input('min') minValue: number = 1;
    @Input('max') maxValue: number = 100;

    plus() {
        this.update(+1);
    }

    minus() {
        this.update(-1);
    }

    private update(delta: number) {
        let nextValue = this.currentValue + delta;
        this.currentValue = Math.min(this.maxValue, Math.max(this.minValue, nextValue));
        this.out.emit(this.currentValue);
    }
}
