import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'kr'
})
export class KrPipe implements PipeTransform {

    transform(value: number, oren: boolean = false): string {
        if (!value) {
            return '0 kr';
        }

        let result: number | string = Number(value);
        result = result.toLocaleString('sv', {
            useGrouping: true,
            minimumFractionDigits: oren ? 2 : 0,
            maximumFractionDigits: oren ? 2 : 0,
        })
        result += ' kr';
        result = result.replace(/\s+/g, '\xA0'); //&nbsp;

        return result;
    }

}
