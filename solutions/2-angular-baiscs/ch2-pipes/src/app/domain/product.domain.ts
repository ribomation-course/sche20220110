export class Product {
    constructor(
        public name: string,
        public price: number,
        public itemsInStore: number,
        public isService: boolean,
        public lastUpdated: Date,
    ) {
    }

    static generate(): Product {
        const names = [
            'apple', 'banana', 'coco nut', 'date plum'
        ];
        const name = names[Math.floor(Math.random() * names.length)];
        const price = Math.random() * 5000;
        const count = Math.random() * 100;
        const svc = Math.random() < .65;
        const date = new Date(Date.now() - Math.floor(Math.random() * 3600 * 24 * 30 * 1000));
        return new Product(name, price, count, svc, date);
    }

}
