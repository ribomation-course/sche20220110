import {Component} from '@angular/core';
import {Product} from "./domain/product.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    products: Product[] = [];

    constructor() {
        const N = 10;
        for (let k = 0; k < N; ++k) {
            this.products.push(Product.generate());
        }
    }

}
