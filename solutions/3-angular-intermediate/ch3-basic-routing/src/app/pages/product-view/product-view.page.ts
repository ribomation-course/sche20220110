import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {Product} from "../../product.domain";
import {ProductDaoService} from "../../services/product-dao.service";
import {Title} from "@angular/platform-browser";

@Component({
    selector: 'app-product-view',
    templateUrl: './product-view.page.html',
    styleUrls: ['./product-view.page.css']
})
export class ProductViewPage implements OnInit {
    product: Product | undefined;

    constructor(
        private route: ActivatedRoute,
        private dao: ProductDaoService,
        private titleSvc: Title
    ) {
    }

    ngOnInit(): void {
        const id = this.route.snapshot.paramMap.get('id');
        this.product = this.dao.one(Number(id));
        if (this.product) {
            this.titleSvc.setTitle(this.product.name)
        }
    }

}
