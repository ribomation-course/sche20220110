import {Component, OnInit} from '@angular/core';
import {Product} from "../../product.domain";
import {ProductDaoService} from "../../services/product-dao.service";
import {Router} from "@angular/router";


@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.page.html',
    styleUrls: ['./product-list.page.css']
})
export class ProductListPage {
    constructor(
        private router: Router,
        public dao: ProductDaoService) {
    }

    async gotoPage(id: number) {
        await this.router.navigate(['/product', id])
    }

   async gotoPage2(p: Product) {
       this.dao.setProduct(p);
        const id = p.id
        await this.router.navigate(['/product', id])
    }
}
