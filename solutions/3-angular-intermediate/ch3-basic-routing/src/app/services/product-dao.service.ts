import {Injectable} from '@angular/core';
import {Product} from "../product.domain";

@Injectable({
    providedIn: 'root'
})
export class ProductDaoService {
    products: Product[] = [];
    private prodInNav: Product | undefined;

    constructor() {
        const names = [
            'apple', 'banana', 'coco nut', 'date plum', 'orange', 'pear', 'kiwi'
        ];
        const N = 10;
        for (let k = 0; k < N; ++k) {
            const id = k + 1;
            const name = names[Math.floor(Math.random() * names.length)];
            const price = 1000 * Math.random();
            this.products.push({id, name, price});
        }
    }

    all(): Product[] {
        return this.products;
    }

    one(id: number): Product | undefined {
        return this.all().find(p => p.id === id);
    }

    setProduct(p: Product) {
        this.prodInNav = p;
    }

    getProduct(): Product | undefined {
        return this.prodInNav;
    }
}
