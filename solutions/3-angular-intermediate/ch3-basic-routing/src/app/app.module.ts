import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HomePage} from './pages/home/home.page';
import {AboutPage} from './pages/about/about.page';
import {ProductListPage} from './pages/product-list/product-list.page';
import {NotFoundPage} from './pages/not-found/not-found.page';
import {Route, RouterModule} from "@angular/router";
import {ProductViewPage} from './pages/product-view/product-view.page';

const routes: Route[] = [
    {path: 'home', component: HomePage},
    {path: 'about', component: AboutPage},
    {path: 'products', component: ProductListPage},
    {path: 'product/:id', component: ProductViewPage},
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: NotFoundPage},
];

@NgModule({
    declarations: [
        AppComponent,
        HomePage,
        AboutPage,
        ProductListPage,
        NotFoundPage,
        ProductViewPage
    ],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
