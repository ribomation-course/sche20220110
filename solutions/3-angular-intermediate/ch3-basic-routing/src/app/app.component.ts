import {Component} from '@angular/core';

interface Nav {
    uri: string;
    text: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    navbar: Nav[] = [
        {uri:'/home', text: 'Hem'},
        {uri:'/products', text: 'Våra Produkter'},
        {uri:'/about', text: 'Om...'},
    ];
}
