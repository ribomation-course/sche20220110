import {Component} from '@angular/core';
import {User} from "./user.domain";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    user: User = {name: '', age: 0,};

    onSubmit(usr: User) {
        console.log(usr);
    }
}
