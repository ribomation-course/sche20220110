import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {User} from "../../user.domain";
import {FormGroup} from "@angular/forms";

@Component({
    selector: 'template-oriented-form',
    templateUrl: './template-oriented-form.widget.html',
    styleUrls: ['./template-oriented-form.widget.css']
})
export class TemplateOrientedFormWidget implements OnInit {
    @ViewChild('form') form: FormGroup | undefined;
    @Input('init') model: User = {name: '', age: 0,};
    @Output('submit') submitEmitter = new EventEmitter<User>();
    @Output('cancel') cancelEmitter = new EventEmitter<any>();

    ngOnInit(): void {
        if (!this.model) {
            throw new Error('missing user model object');
        }
    }

    submit() {
        this.submitEmitter.emit(this.model);
        this.form?.reset();
        this.form?.setValue({name: '', age: 0,});
    }

    cancel() {
        this.cancelEmitter.emit()
    }
}
