import {Component} from '@angular/core';
import {environment as env} from '../../../environments/environment'

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss']
})
export class WelcomePage {
    readonly image = env.welcome.imageUrl
}
