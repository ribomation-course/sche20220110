import {Component} from '@angular/core';

@Component({
    selector: 'app-welcome',
    templateUrl: './welcome.page.html',
    styleUrls: ['./welcome.page.scss']
})
export class WelcomePage {
    readonly image = 'http://localhost:3000/img/store.jpg';
}
