export class Product {
    constructor(
        public id:string, 
        public name:string, 
        public price:number, 
        public city:string, 
        ) {}

    static fromDB(data:any):Product {
      return new Product(data.id, data.name, data.price, data.city);
    }

    toDB():any {
      return Object.assign({}, this);
    }
  }
  