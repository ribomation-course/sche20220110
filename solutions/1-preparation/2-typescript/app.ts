import {readFileSync, writeFileSync} from 'fs';
import {Product} from './product';

const json     = readFileSync('./products.json').toString();
const products = JSON.parse(json).map(p => Product.fromDB(p));
console.log('loaded %d products', products.length);

const result   = products.filter(p => p.price >= 1100).map(p => p.toDB());
console.log('# expensive products = %d', result.length);

const outfile = 'products-expensive.json';
writeFileSync(outfile, JSON.stringify(result,null,2));
console.log('written %s', outfile);
